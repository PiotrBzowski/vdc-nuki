vdc-??

vdc-?? implements virtual device connector (vdc) for ?? device to digitalSTROM systems.

vdc-?? is based on a generic C++ framework called p44vdc which is included as a submodule into this project.

vdc-doorbird also makes use of a set of generic C++ utility classes called p44utils, which provides basic mechanisms for mainloop-based, nonblocking I/O driven automation daemons. p44utils is also included as a submodule into this project.

